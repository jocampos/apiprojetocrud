<?php

namespace App\Http\Controllers;

use Faker\Provider\File;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    //

    public function upload(Request $request){

        if($request->allFiles()){
            $file = $request->file('foto');
            $path = "upload";
            $ext = ($request->file('foto')->getClientOriginalExtension())? $request->file('foto')->getClientOriginalExtension() : $request->file('foto')->extension();
            $filename = md5(uniqid(rand(), true)).".".$ext;

            if($file->move($path, $filename)){
                return response()->json([
                    'message' => 'Imagem gravada com sucesso',
                    'img' => env("APP_URL")."/".$path."/".$filename
                ], 200);
            }

            return response()->json([
                'message' => 'Imagem  nao pode ser gravada'
            ], 400);

        }
        return response()->json([
            'error' => 'Imagem não encontrada'
        ], 404);
    }

    public function delete(Request $request){

        if($request->foto){
            $img = explode(env('APP_URL'), $request->foto);


            if(unlink(public_path().$img[1])){
                return response()->json([
                    'message' => 'Imagem deletada'
                ], 200);
            }

            return response()->json([
                'message' => 'Não foi possível enviar a imagem.'
            ], 400);
        }

        return response()->json([
            'error' => 'Recurso não encontrado'
        ], 404);


    }

}
